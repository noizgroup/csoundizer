from ubuntu:bionic
RUN apt-get update && apt-get install -y python-csound
WORKDIR /root
COPY noiz.py noiz.py
CMD [ "/bin/bash" ]
