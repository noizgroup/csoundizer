# SquareWave.py
import csnd6
orc = """
nchnls=2
instr   1 
  aoutl  rand 32000,.42,1
  aoutr = aoutl
  outs aoutl,aoutr
endin
"""
sco ="""
i 1 0 15
"""
c = csnd6.Csound()
c.SetOption("-onoiz.wav") 
c.CompileOrc(orc)
c.ReadScore(sco) 
c.Start()
c.Perform() 
c.Stop()